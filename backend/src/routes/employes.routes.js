const { Router } = require('express')
const router = Router()

const employeesCtrl = require('../controllers/employes.controllers.js')

router.get('/', employeesCtrl.getEmployees);

router.post('/', employeesCtrl.createEmployees);

router.get('/:id', employeesCtrl.getEmployees);

router.get('/:id', employeesCtrl.editEmployees);

router.get('/:id', employeesCtrl.deleteEmployees);

module.exports = router